#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
Created on Mon Dec 9 23:19:30 2015
@author: Larissa,olmerg
copyright 2015  Olmer Garcia  olmerg@gmail.com
Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

"""

import math
import time
import json
import rospy
import sys
import os
from sensor_msgs.msg import Imu, NavSatFix, NavSatStatus
from nav_msgs.msg   import Odometry
from geometry_msgs.msg   import TwistWithCovarianceStamped
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, String
import message_filters
import Queue
from geodesy import utm

info_gps=[]
localization_pc=[] 
pub_localization=[]

def gpsall(gps_data,gps_vel):
	utm_data=utm.fromLatLong(gps_data.latitude,gps_data.longitude,gps_data.altitude)
	vel=math.sqrt(gps_vel.twist.twist.linear.y**2+gps_vel.twist.twist.linear.x**2) 
	angle=180*math.atan2(gps_vel.twist.twist.linear.x,gps_vel.twist.twist.linear.y)/math.pi
	info_gps[0]= rospy.get_time()
	info_gps[1]= 1	
	info_gps[2]= utm_data.northing
	info_gps[3]= utm_data.easting
	info_gps[4]= angle
	info_gps[5]= vel

#create the vector of the data
def init_data():
	global info_gps
	global localization_pc
	global pub_localization
	info_gps=[]
	for i in range(0,6):
		info_gps.append(0)
	localization_pc=Odometry()
	localization_pc.header.seq=0
	pub_localization = rospy.Publisher('localization_pc', Odometry, queue_size=1)

def timer_callback(event):
    #verificar a atualizacao do sensor
    # calcular o filtro de kalman
    #publicar o resultado
    print info_gps[5]
    localization_pc.header.stamp=rospy.Time.now()
    localization_pc.header.seq=localization_pc.header.seq+1
    localization_pc.header.frame_id="T_car"
    localization_pc.pose.pose.position.x=info_gps[2]
    localization_pc.pose.pose.position.y=info_gps[3]
    # errado
    localization_pc.pose.pose.orientation.z=info_gps[4]
    localization_pc.twist.twist.linear.x=info_gps[5]
    pub_localization.publish(localization_pc)
	

if __name__ == '__main__':
# Create a TCP/IP socket
    
    rospy.init_node('vilma_localization_node', anonymous=True)
    init_data()
    
    #tempo de ciclo do timer
    timer_time=rospy.get_param('timer_time', 0.2)  

    gps_sub = message_filters.Subscriber('/vilma_ublox/fix', NavSatFix)
    velgps_sub = message_filters.Subscriber('/vilma_ublox/fix_velocity', TwistWithCovarianceStamped)
    ts = message_filters.TimeSynchronizer([gps_sub, velgps_sub], 10)
    ts.registerCallback(gpsall)

    rospy.Timer(rospy.Duration(timer_time), timer_callback)
    rospy.spin()
 
