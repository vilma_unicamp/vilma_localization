this package contain the VILMA  localization program. the next programs are implemented:

- **vilma_localization_node** This take the information of gps, gyroscope and wheel speed to generate an odometry ROS message .

This program has one launch  to test the node using a bag file.
```
    roslaunch vilma_localization vilma_localization_test.launch bagfile:="/home/larissa/curva_20kmh_2015-11-10-21-02-21.bag"


```
